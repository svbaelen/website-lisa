import { rocketLaunch } from '@rocket/launch';
import { presetRocketSearch } from '@rocket/search';

export default /** @type {import('@rocket/cli').RocketCliOptions} */ ({
  absoluteBaseUrl: 'https://mappalink.gitlab.io/management/website/ml-public/',
  presets: [rocketLaunch(), presetRocketSearch()],
});
