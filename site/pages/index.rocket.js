/* START - Rocket auto generated - do not touch */
export const sourceRelativeFilePath = 'index.rocket.js';
import { html, setupUnifiedPlugins, components } from './recursive.data.js';
export { html, setupUnifiedPlugins, components };
export async function registerCustomElements() {
  // server-only components
  customElements.define('rocket-header', await import('#src/components/RocketHeader.js').then(m => m.RocketHeader));
  // hydrate-able components
  customElements.define('rocket-search', await import('@rocket/search/search.js').then(m => m.RocketSearch));
  customElements.define('rocket-drawer', await import('#src/components/RocketDrawer.js').then(m => m.RocketDrawer));
}
export const needsLoader = true;
/* END - Rocket auto generated - do not touch */

import { nothing } from 'lit';
import { LayoutHome } from '@rocket/launch';
//import { LayoutHome } from '#src/layouts/LayoutLisa.js';
import { layoutData } from '#src/layouts/layoutData.js';

export const layout = new LayoutHome({
  ...layoutData,
  //header__40: () => nothing, // search (see site/src/layouts/layoutData.js)
  //drawer__30: () => nothing, // search (see site/src/layouts/layoutData.js)
  headerDarkBackground: false,
  headerHideLogo: true,
});

export default () => html`
  <script type="module">
    /*
    const btn = document.querySelector("#bestel-nu-btn");
    btn.addEventListener("mouseover", () => {
      const btnSvg = document.querySelector("#bestel-nu-svg");
        btnSvg.setAttribute("stroke", "var(--primary-color-darker)");
      }
    );
    */
  </script>
  <div id="ml-title-meta-container">
    <div>
      <h1 id="ml-title">Woeste Grond</h1>
    </div>
    <div id="ml-subtitle-container">
      <div class="ml-subtitle-container-item subtitle-text">
        <h1 id="ml-subtitle" class="with-logo">
          <span><a href="/winkel/">Seizoenssmaken</a> &amp; <a href="/blog/">Veldverhalen</a>
        </h1>
      </div>
    </div>
  </div>
  <a id="bestel-nu-btn" class="btn-down" href="#">
    <span style="font-weight:800;font-size:1.45em;">Bestel</span>
    <br>
    tappenades voor het eindejaar
    <br>
    <!--
    <svg  class="svg-btn"  style="transform: rotate(180deg);" width="45" height="45" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="24" cy="24" r="20" fill="none" stroke="#333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
      <path id="bestel-nu-svg" d="M12 31L24 11L36 31H12Z" fill="var(--primary-text-color)" stroke="var(--primary-text-color)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
    -->
  </a>

  <!-- first row -->
  <div id="about" class="ml-container-flexcol-center">
    <div class="ml-flexcol-item ml-container-flexrow-center ml-flexrow-100">
      <div class="ml-flexrow-item rounded">
        <img src="resolve:#src/assets/images/pompoenen.jpg" alt="pompoenen">
      </div>
      <div class="ml-flexrow-item about-text">
        <!-- Why? -->
        <p>
        <!--Mappalink envisages a future where automated <b>machines</b> and <b>humans</b> can meaningfully <b>co-exist</b> and co-operate.-->
        <!--Meaningful <b>robot</b> and human <b>co-existence</b> and <b>co-operation</b>.-->
        </p>
        <!-- How? -->
        <p>
        We create <b>intuitive</b> and <b>user-friendly</b> automation systems via <b>custom-made integrations</b> of our products and services into <i>your</i> application stack.
        We exploit <b>open</b> ICT <b>standards</b> to offer full <b>customer flexibility</b> and avoid ecosystem lock-ins.
        </p>
        <!-- What? -->
        <p>
        Mappalink <b>translates</b> state-of-the-art <b>research</b> into real world industrial <b>applications</b>.
        We automate and coordinate complex <a target="_blank" href="https://en.wikipedia.org/wiki/Cyber-physical_system">cyber-physical systems</a>,
        where we focus on <b>explainability</b> and <b>reconfigurability</b> at runtime: enabling <b>intuitive</b>, <b>sustainable</b>, and <b>maintainable</b> applications.
        </p>
        <p>
        <a target="_blank" href="https://gerbenpeeters.org/">Gerben Peeters</a> and
        <a target="_blank" href="https://svbaelen.me">Senne Van Baelen</a> founded Mappalink BV in October 2022. After working together for over 6 years at the
        <a target="_blank" href="https://www.mech.kuleuven.be/imp/">KU Leuven Robotics
          group</a>, they followed their mutual interest for <b>integrating</b> innovative research into <b>real life</b> applications.
        </p>
      </div>
    </div>
  </div>
`;
