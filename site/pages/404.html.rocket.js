/* START - Rocket auto generated - do not touch */
export const sourceRelativeFilePath = '404.html.rocket.js';
import { html, setupUnifiedPlugins, components } from './recursive.data.js';
export { html, setupUnifiedPlugins, components };
/* END - Rocket auto generated - do not touch */

import { Layout404 } from '@rocket/launch';

export const layout = new Layout404();

export default () => '';
