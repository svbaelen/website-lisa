/* START - Rocket auto generated - do not touch */
export const sourceRelativeFilePath = '80--blog/index.rocket.js';
import { html, layout, setupUnifiedPlugins, components, menuLinkText } from './local.data.js';
export { html, layout, setupUnifiedPlugins, components, menuLinkText };
export async function registerCustomElements() {
  // server-only components
  customElements.define('rocket-header', await import('#src/components/RocketHeader.js').then(m => m.RocketHeader));
  customElements.define('launch-blog-overview', await import('#src/components/RocketLaunchBlogOverview.js').then(m => m.LaunchBlogOverview));
  // hydrate-able components
  customElements.define('rocket-search', await import('@rocket/search/search.js').then(m => m.RocketSearch));
  customElements.define('rocket-drawer', await import('#src/components/RocketDrawer.js').then(m => m.RocketDrawer));
}
export const needsLoader = true;
/* END - Rocket auto generated - do not touch */

import { layoutData } from '#src/layouts/layoutData.js';

export default () => html`
  <h1 style="display:none">Blog</h1>
  <div id="blog" class="ml-container-flexcol-center">
    <div id="" class="ml-flexcol-item ml-head">
      <span class="ml-lighter ml-head-item">Blog Overview</span>
    </div>
    <div id="" class="ml-flexcol-item ml-subsubhead">
      <span class="ml-head-item">Find out more details of our developments in the following blog posts.</span>
    </div>
    <div id="blog-overview" class="ml-flexcol-item">
      <launch-blog-overview
          .pageTree=${layoutData.pageTree}
          .sourceRelativeFilePath=${sourceRelativeFilePath}
          >
      </launch-blog-overview>
    </div>
  </div>
  </div>
`;
