import { LayoutBlogPost } from '#src/layouts/LayoutBlogPost.js';
import { layoutDataBlog } from '#src/layouts/layoutDataBlog.js';

export { html, setupUnifiedPlugins, components } from "../recursive.data.js";

export const menuLinkText = 'Blog';

export const layout = new LayoutBlogPost({
  ...layoutDataBlog,
});
