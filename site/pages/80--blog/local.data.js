import { LayoutHome } from '@rocket/launch';
import { layoutData } from '#src/layouts/layoutData.js';
import { components as initComponents } from "../recursive.data.js";

export { html, setupUnifiedPlugins } from "../recursive.data.js";
export const menuLinkText = 'Blog';
export const layout = new LayoutHome({
  ...layoutData,
  //header__40: () => nothing, // search (see site/src/layouts/layoutData.js)
  //drawer__30: () => nothing, // search (see site/src/layouts/layoutData.js)
  headerDarkBackground: false,
  headerHideLogo: true,
});

export const components = {
  ...initComponents,
  'launch-blog-overview': '#src/components/RocketLaunchBlogOverview.js::LaunchBlogOverview',
  'launch-blog-preview': '#src/components/RocketLaunchBlogPreview.js::LaunchBlogPreview',
};
