/* START - Rocket auto generated - do not touch */
export const sourceRelativeFilePath = '60--contact/index.rocket.js';
import { html, setupUnifiedPlugins, components } from '../recursive.data.js';
export { html, setupUnifiedPlugins, components };
export async function registerCustomElements() {
  // server-only components
  customElements.define('rocket-header', await import('#src/components/RocketHeader.js').then(m => m.RocketHeader));
  // hydrate-able components
  customElements.define('rocket-search', await import('@rocket/search/search.js').then(m => m.RocketSearch));
  customElements.define('rocket-drawer', await import('#src/components/RocketDrawer.js').then(m => m.RocketDrawer));
}
export const needsLoader = true;
/* END - Rocket auto generated - do not touch */

import { nothing } from 'lit';
import { LayoutHome } from '@rocket/launch';
import { layoutData } from '#src/layouts/layoutData.js';

export const menuLinkText = 'Contact';

export const layout = new LayoutHome({
  ...layoutData,
  //header__40: () => nothing, // search (see site/src/layouts/layoutData.js)
  //drawer__30: () => nothing, // search (see site/src/layouts/layoutData.js)
  headerDarkBackground: false,
  headerHideLogo: false,
  //footer__100: () => nothing,
});

export default () => html`
  <script type="module">
    const params = new Proxy(new URLSearchParams(window.location.search), {
      get: (searchParams, prop) => searchParams.get(prop),
    });

    const selector = document.querySelector("#subject");

    function setContact(val) {
      const msg = document.querySelector("#message");

      if (document.querySelector('option[value="' + val + '"]')){
        selector.value = val;
        let date = "";
        if (val.substring(0,5) ===  "dec24")
          date = "24 december (tussen 9u00 en 12u00)";
        else if (val.substring(0,5) ===  "dec25")
          date = "25 december (tussen 9u00 en 12u00)";
        else if (val.substring(0,5) ===  "dec31")
          date = "31 december (tussen 9u00 en 12u00)";

        if (date){
          msg.value = "Ik had graag ... portie(s) tappenade trio besteld.\\n\
\\n\
Ik zal deze komen afhalen op " + date + ". Tot dan!";
        }
        else
          msg.value = "";
      }
    }

    setContact(params.subject);

    selector.addEventListener('change', (event) => {
      setContact(event.target.value);
    });
  </script>

  <h1 style="display:none">Contacteer ons</h1>
  <div id="about" class="ml-container-flexcol-center">
    <div id="contact-form" class="ml-flexcol-item">
      <h1>Neem contact op</h1>
      <br>
      <form method="post" action="https://api.staticforms.xyz/submit" method="post">
        <input type="hidden" name="accessKey" value="5293c651-297b-4ad7-8d26-3f141aacdb53">
        <div class="fields">
          <div class="field">
            <label for="email">Email*</label>
            <input type="email" name="email" id="email" required/>
          </div>
          <div class="field half">
            <label for="name">Naam</label>
            <input type="text" name="name" id="name"/>
          </div>
          <div class="field half">
            <label for="phone">Telefoonnummer</label>
            <input type="text" name="phone" id="phone"/>
          </div>
          <div class="field half">
            <label for="subject">Onderwerp*</label>
            <div class="select">
              <select name="subject" id="subject"/>
                <option value="algemeen">Algemene vraag</option>
                <option value="dec24">Bestelling 24 december</option>
                <option value="dec25">Bestelling 25 december</option>
                <option value="dec31">Bestelling 31 december</option>
              </select>
            </div>
          </div>
          <div class="field">
            <label for="message">Bericht*</label>
            <textarea name="message" id="message" rows="4" required></textarea>
          </div>
        </div>
        <input type="hidden" name="replyTo" value="@"> <!-- Optional -->
        <!--<input type="hidden" name="redirectTo" value="http://localhost:8000/bedankt">-->
        <input type="hidden" name="redirectTo" value="http://localhost:8000/bedankt/">
        <input type="submit" value="Verstuur" class="primary"/>
        <br>
        <p style="font-size:0.8em">* = verplicht veld</p>
      </form>
    </div>
  </div>
`
