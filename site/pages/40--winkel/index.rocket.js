/* START - Rocket auto generated - do not touch */
export const sourceRelativeFilePath = '40--winkel/index.rocket.js';
import { html, setupUnifiedPlugins, components } from '../recursive.data.js';
export { html, setupUnifiedPlugins, components };
export async function registerCustomElements() {
  // server-only components
  customElements.define('rocket-header', await import('#src/components/RocketHeader.js').then(m => m.RocketHeader));
  // hydrate-able components
  customElements.define('rocket-search', await import('@rocket/search/search.js').then(m => m.RocketSearch));
  customElements.define('rocket-drawer', await import('#src/components/RocketDrawer.js').then(m => m.RocketDrawer));
}
export const needsLoader = true;
/* END - Rocket auto generated - do not touch */

import { nothing } from 'lit';
import { LayoutHome } from '@rocket/launch';
import { layoutData } from '#src/layouts/layoutDataBg2.js';

export const menuLinkText = 'Winkel';

export const layout = new LayoutHome({
  ...layoutData,
  //header__40: () => nothing, // search (see site/src/layouts/layoutData.js)
  //drawer__30: () => nothing, // search (see site/src/layouts/layoutData.js)
  headerDarkBackground: false,
  headerHideLogo: false,
});

export default () => html`
  <div id="winkel" class="ml-container-flexcol-center">
    <div id="binnenkort" class="ml-flexcol-item">
      <h1>Binnenkort!</h1>
      <p>
      Onze webwinkel opent <b>begin 2023</b>.
      <br><br>
      Volg ons verhaal op de voet via <a target="_blank" href="https://www.instagram.com/woestegrond.be/">instagram</a>: ontdek samen met ons,
      bewonder kiekjes vanop het veld, en nog veel meer!
      </p>
    </div>
  </div>
`;
