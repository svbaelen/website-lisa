/* START - Rocket auto generated - do not touch */
export const sourceRelativeFilePath = '50--over/index.rocket.js';
import { html, setupUnifiedPlugins, components } from '../recursive.data.js';
export { html, setupUnifiedPlugins, components };
export async function registerCustomElements() {
  // server-only components
  customElements.define('rocket-header', await import('#src/components/RocketHeader.js').then(m => m.RocketHeader));
  // hydrate-able components
  customElements.define('rocket-search', await import('@rocket/search/search.js').then(m => m.RocketSearch));
  customElements.define('rocket-drawer', await import('#src/components/RocketDrawer.js').then(m => m.RocketDrawer));
}
export const needsLoader = true;
/* END - Rocket auto generated - do not touch */

import { nothing } from 'lit';
import { LayoutHome } from '@rocket/launch';
import { layoutData } from '#src/layouts/layoutDataBg2.js';

export const menuLinkText = 'Over';

export const layout = new LayoutHome({
  ...layoutData,
  //header__40: () => nothing, // search (see site/src/layouts/layoutData.js)
  //drawer__30: () => nothing, // search (see site/src/layouts/layoutData.js)
  headerDarkBackground: false,
  headerHideLogo: false,
});

export default () => html`
  <h1 style="display:none">About</h1>
  <div id="about" class="ml-container-flexcol-center">
    <!--<h2 class="ml-flexcol-item ml-page-main-title">About us</h2>-->
    <div class="ml-flexcol-item">
        <p class="ml-head">
        <!--
        <span class="ml-head-item ml-lighter">Linking</span>
        <span class="ml-head-item">innovative research with</span>
        <span class="ml-head-item">industrial development.</span>
        -->
        <span class="ml-head-item">Bodem.</span>
        <span class="ml-head-item">Boer.</span>
        <span class="ml-head-item">Bord.</span>
        <!--<span class="ml-head-3">and societal needs.</span>-->
        </p>
    </div>
    <!-- first row -->
    <div id="about-ml" class="ml-flexcol-item ml-container-flexrow-center ml-flexrow-100">
      <div id="img-about-lisa" class="ml-flexrow-item rounded-uneven">
        <img src="./img/lisa.jpg" alt="img">
      </div>
      <div class="ml-flexrow-item about-text">
        <!-- Why? -->
        <p>
        <!--Mappalink envisages a future where automated <b>machines</b> and <b>humans</b> can meaningfully <b>co-exist</b> and co-operate.-->
        <!--Meaningful <b>robot</b> and human <b>co-existence</b> and <b>co-operation</b>.-->
        </p>
        <!-- How? -->
        <p>
        We create <b>intuitive</b> and <b>user-friendly</b> automation systems via <b>custom-made integrations</b> of our products and services into <i>your</i> application stack.
        We exploit <b>open</b> ICT <b>standards</b> to offer full <b>customer flexibility</b> and avoid ecosystem lock-ins.
        </p>
        <!-- What? -->
        <p>
        Mappalink <b>translates</b> state-of-the-art <b>research</b> into real world industrial <b>applications</b>.
        We automate and coordinate complex <a target="_blank" href="https://en.wikipedia.org/wiki/Cyber-physical_system">cyber-physical systems</a>,
        where we focus on <b>explainability</b> and <b>reconfigurability</b> at runtime: enabling <b>intuitive</b>, <b>sustainable</b>, and <b>maintainable</b> applications.
        </p>
        <p>
        <a target="_blank" href="https://gerbenpeeters.org/">Gerben Peeters</a> and
        <a target="_blank" href="https://svbaelen.me">Senne Van Baelen</a> founded Mappalink BV in October 2022. After working together for over 6 years at the
        <a target="_blank" href="https://www.mech.kuleuven.be/imp/">KU Leuven Robotics
          group</a>, they followed their mutual interest for <b>integrating</b> innovative research into <b>real life</b> applications.
        </p>
      </div>
    </div>

    <!-- second row -->
    <div id="about-products" class="ml-flexcol-item ml-container-flexrow-center">
      <a href="/winkel/" class="link-box">
        <div id="" class="ml-flexrow-item ml-container-flexcol-center">
          <div class="ml-flexcol-item rounded img-box">
            <img src="./img/pompoenen.jpg" alt="mappalink">
          </div>
          <div class="ml-flexcol-item">
            <p class="link-box-text">
            Ontdek en proef onze heerlijke seizoenssmaken.
            </p>
          </div>
        </div>
      </a>
      <a href="/blog/" class="link-box">
        <div id="" class="ml-flexrow-item ml-container-flexcol-center">
          <div class="ml-flexcol-item rounded img-box">
            <img src="./img/veld.jpg" alt="mappachains">
          </div>
          <div class="ml-flexcol-item">
            <p class="link-box-text">
            Duik in één van onze inspirerende veldverhalen.
            </p>
          </div>
        </div>
      </a>
    </div>

    <!-- COMPONENTS (mappachains) -->
    <!-- WE CONNECT (mappalinks) -->
  </div>
`;
