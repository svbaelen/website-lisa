import { LayoutSidebar } from '@rocket/launch';
import { adjustPluginOptions } from 'plugins-manager';
import { mdjsSetupCode } from '@mdjs/core';
import { html } from 'lit';
import { rocketComponents } from '@rocket/components/components.js';
import { searchComponents } from '@rocket/search/components.js';
import htmlHeading from 'rehype-autolink-headings';
import { launchComponents } from '@rocket/launch/components.js';
import { layoutData, pageTree } from '#src/layouts/layoutData.js';

export { html };

export const layout = new LayoutSidebar({
  ...layoutData,
});

export const setupUnifiedPlugins = [
  adjustPluginOptions(mdjsSetupCode, {
    simulationSettings: {
      simulatorUrl: '/simulator/',
    },
  }),
  // this adds an octicon to the headlines
  adjustPluginOptions(htmlHeading, {
    properties: {
      className: ['anchor'],
    },
    content: [
      {
        type: 'element',
        tagName: 'svg',
        properties: {
          className: ['octicon', 'octicon-link'],
          viewBox: '0 0 16 16',
          ariaHidden: 'true',
          width: 16,
          height: 16,
        },
        children: [
          {
            type: 'element',
            tagName: 'path',
            properties: {
              fillRule: 'evenodd',
              d: 'M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z',
            },
          },
        ],
      },
    ],
  }),
];

export const components = {
  ...rocketComponents,
  ...searchComponents,
  ...launchComponents,
  'ml-rotating-text': '#src/components/MlRotatingText.js::MlRotatingText',
  'ml-img-slider': '#src/components/MlImgSlider.js::MlImgSlider',
  'rocket-content-area': '#src/components/RocketContentArea.js::RocketContentArea',
  'rocket-header': '#src/components/RocketHeader.js::RocketHeader',
  'rocket-main-docs': '#src/components/RocketMainDocs.js::RocketMainDocs',
  'inline-notification': '#src/components/InlineNotification.js::InlineNotification',
  'dd-carousel': '#src/components/DdCarousel.js::DdCarousel',
  'rocket-drawer': '#src/components/RocketDrawer.js::RocketDrawer',
  'launch-blog-details': '#src/components/RocketLaunchBlogDetails.js::LaunchBlogDetails',
};
