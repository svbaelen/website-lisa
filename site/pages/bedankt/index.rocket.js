/* START - Rocket auto generated - do not touch */
export const sourceRelativeFilePath = 'bedankt/index.rocket.js';
import { html, setupUnifiedPlugins, components } from '../recursive.data.js';
export { html, setupUnifiedPlugins, components };
export async function registerCustomElements() {
  // server-only components
  customElements.define('rocket-header', await import('#src/components/RocketHeader.js').then(m => m.RocketHeader));
  // hydrate-able components
  customElements.define('rocket-search', await import('@rocket/search/search.js').then(m => m.RocketSearch));
  customElements.define('rocket-drawer', await import('#src/components/RocketDrawer.js').then(m => m.RocketDrawer));
}
export const needsLoader = true;
/* END - Rocket auto generated - do not touch */

import { nothing } from 'lit';
import { LayoutHome } from '@rocket/launch';
import { layoutData } from '#src/layouts/layoutDataBg2.js';

export const menuExclude = true;

export const layout = new LayoutHome({
  ...layoutData,
  //header__40: () => nothing, // search (see site/src/layouts/layoutData.js)
  //drawer__30: () => nothing, // search (see site/src/layouts/layoutData.js)
  headerDarkBackground: false,
  headerHideLogo: false,
});

export default () => html`
  <script>
    setTimeout(() => {
          window.location.href = '/';
    }, 7000);
  </script>
  <h1 style="display:none">Bedankt!</h1>
  <div id="bedankt" class="ml-container-flexcol-center">
    <div id="bericht" class="ml-flexcol-item">
      <h1 style="line-height:1em;">Bedankt voor je bericht!</h1>
      <p>We beantwoorden je vraag of bestelling zo spoedig mogelijk.</p>
      <p>(&rarr;je wordt automatisch doorverwezen naar de homepagina na 5 seconden)</p>
    </div>
  </div>
`;
