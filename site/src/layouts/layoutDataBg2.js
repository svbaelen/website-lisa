import { html } from 'lit';

import { layoutData as layoutDataO } from '#src/layouts/layoutData.js';

export const layoutData = {
  ...layoutDataO,
  head__45: html`<link rel="stylesheet" href="resolve:#src/assets/css/ml_body_2.css" /> `,
};
