import { html } from 'lit';

import { PageTree } from '@rocket/engine';
import { IndexMenu } from '@rocket/engine';


export const pageTree = new PageTree();
await pageTree.restore(new URL('../../pages/pageTreeData.rocketGenerated.json', import.meta.url));

const search = html`
  <rocket-search
    loading="hydrate:onFocus"
    class="search"
    json-url="/rocket-search-index.json"
    slot="search"
  ></rocket-search>
`;

const foot = html``
const footerMenu = [
  {
    name: 'Woeste Grond',
    children: [
      {
        text: html`<a href="https://goo.gl/maps/J47PGChbmQr8AY7m6">Vlamingenstraat 10</a>`,
      },
      {
        text: '2400 Mol',
      },
    ],
  },
  {
    name: 'Contact',
    children: [
      {
        text: html`<a href="mailto:lisa@woestegrond.be">lisa@woestegrond.be</a>`,
      },
    ],
  },

  {
    name: 'Ontdek',
    children: [
      {
        text: html`
          <div id="footer-disco-images">
            <a target="_blank" href="https://www.instagram.com/woestegrond.be/">
              <img class="footer-disco-img" src="resolve:#src/assets/instagram.svg" alt="mappalink"/>
            </a>
          </div>
        `,
      },
    ],
  },
];

export const layoutData = {
  pageTree,

  /* Rubik is already built-in and preloaded */
  head__64: html`<link rel="preload" href="/fonts/29LMakina-Light.woff2" as="font" type="font/woff2" crossorigin />`,
  head__63: html`<link rel="preload" href="/fonts/29LMakina-Regular.woff2" as="font" type="font/woff2" crossorigin />`,
  head__62: html`<link rel="preload" href="/fonts/Sora-VariableFont_wght.woff2" as="font" type="font/woff2" crossorigin />`,
  head__62: html`<link rel="preload" href="/fonts/Rosario-VariableFont_wght.woff2" as="font" type="font/woff2" crossorigin />`,

  head__43: html`<link rel="stylesheet" href="resolve:#src/assets/css/fonts.css" /> `,
  head__42: html`<link rel="stylesheet" href="resolve:#src/assets/css/rocket_variables.css" /> `,
  head__41: html`<link rel="stylesheet" href="resolve:#src/assets/css/ml.css" /> `,

  description: 'Seizoenssmaken en veldverhalen // bodem, boer, bord',

  // always assume you're in the root folder for mobile menu, since to nested
  // content needs to be shown in the mobile menu
  drawer__50: data => pageTree.renderMenu(new IndexMenu(), "index.rocket.js"),

  header__40: search,
  drawer__30: search,
  //headerHideLogo: false,
  //logoSrc: '/icon.svg',
  //logoAlt: 'Rocket Logo',
  // contant__650 to remove 'found a mistake edit on github'
  content__650: foot,
  logoSmall: html``,
  siteName: "Woeste Grond",
  socialLinks: [],
  //footerMenu: footerMenu,
  // override from node_modules/@rocket/launch/src/LayoutMain.js, no href
  footer__100: html`
    <div id="footer-menu">
      ${footerMenu.map(
        category => html`
          <nav>
            <h3>${category.name}</h3>
            <ul>
              ${category.children.map(
                entry => html`
                  <li>
                    <p>${entry.text}</p>
                  </li>
                `,
              )}
            </ul>
          </nav>
        `,
      )}
    </div>
  `,
};
