import { html } from 'lit';

import { layoutData } from './layoutData.js';

export const layoutDataBlog = {
  ...layoutData,
  head__45: html`<script defer type="module" src="resolve:#src/client-scripts/indexBlog.js"></script>`,
};
