import { LitElement, html, css } from "lit";
import { classMap } from "lit/directives/class-map.js";

export class MlRotatingText extends LitElement {
  static properties = {
    items: { type: Array },
    inIndex: { type: Number, reflect: true, attribute: "in-index" },
    letterSpacing: { type: Array, reflect: true, attribute: "letter-spacing" },
    interval: { type: Number, reflect: true, attribute: "interval" },
    customInterval: { type: Boolean, reflect: true, attribute: "custom-interval" },
  };

  constructor() {
    super();
    this.initIndex = 0;
    this.inIndex = 0;
    this.outIndex = -1;
    this.interval = 2000;
    this.customInterval = false;
  }

  updateSpacing(space, delay) {
    setTimeout(() => {
      this.style.setProperty("--p-letter-spacing", space);
    }, delay);
  }

  next() {
    this.updateSpacing(`${this.letterSpacing[this.inIndex + 1]}em`, 300);
    this.outIndex = this.inIndex;
    if (this.inIndex < this.items.length - 1) {
      this.inIndex += 1;
    } else {
      this.inIndex = 0;
    }
  }

  firstUpdated() {
    if (!this.letterSpacing) this.letterSpacing = new Array(this.items.length).fill(0);
    this.initIndex = -1;
    this.style.setProperty("--p-letter-spacing", `${this.letterSpacing[0]}px`);
    if (!this.customInterval){
      clearInterval(this.timer);
      this.timer = setInterval(() => {
        this.next();
      }, this.interval);
    }
  }

  render() {
    return html`
        ${this.items.map(
          (item, index) =>
          html`<span
            id="animation"
            class=${classMap({
              "anim-in": index !== this.initIndex && index === this.inIndex,
              "anim-out": index === this.outIndex,
              "anim-static": index === this.initIndex,
            })}
            >
            ${item}
          </span>`
        )}
    `;
  }

  static styles = [
    css`
      :host {
        --p-letter-spacing: 0px;
        position: relative;
        overflow: hidden;
        display: block;
        height: 1em;
        line-height: inherit;
      }

      #animation {
        position: absolute;
        top: 0;
        margin:0;
        transform: translate3d(0, -120%, 0);
        color: inherit;
        letter-spacing: var(--p-letter-spacing);
      }

      slot {
        display: inline;
      }

      @keyframes textAnimIn {
        0% {
          transform: translate3d(0, -120%, 0);
        }

        100% {
          transform: translate3d(0, 0%, 0);
        }
      }

      @keyframes textAnimOut {
        0% {
          transform: translate3d(0, 0%, 0);
        }

        50% {
          transform: translate3d(0, -20%, 0);
        }

        100% {
          transform: translate3d(0, 120%, 0);
        }
      }

      .anim-static {
        transform: translate3d(0, 0, 0);
      }

      .anim-in {
        animation: textAnimIn 0.6s 0.3s forwards;
      }

      .anim-out {
        animation: textAnimOut 0.6s forwards;
      }
    `,
  ];
}
