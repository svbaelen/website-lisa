import { html, css, LitElement, nothing } from 'lit';

export class LaunchBlogPreview extends LitElement {
  static properties = {
    post: { type: Object },
  };

  constructor() {
    super();
    /** @type {{ publishDate: Date; title: string; description: string; url: string; name: string; } | undefined} */
    this.post = undefined;
    this.dateFormatter = new Intl.DateTimeFormat('en-US', {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
    });
  }

  render() {
    if (!this.post) {
      return nothing;
    }
    return html`
      <article class="post-preview">
        <a href="${this.post.url}" class="link-box">
          <div id="img-box" class="flexcol-center">
            <div class="flexcol-item rounded img-box">
              <img src="${this.post.img}" alt="can't find ${this.post.img}">
            </div>
            <div class="flexcol-item flexcol-text">
              <header>
                <h1 class="title">${this.post.name}</h1>
                <p class="publish-date">${this.dateFormatter.format(this.post.publishDate)}</p>
              </header>
              <p id="description">${this.post.description}</p>
            </div>
        </div>
      </a>
      </article>
    `;
  }

  static styles = [
    css`
      :host {
        --max-width:100%;
        --text-font-size:0.80em;
        --title-font-size: calc(var(--text-font-size) * 1.35);
        --date-font-size: calc(var(--text-font-size) * 1.1);
        --flex-colheight: 180px;
      }
      @media only screen and (min-width: 400px) {
          :host {
              --text-font-size:0.80em;
          }
      }
      @media only screen and (min-width: 440px) {
          :host {
              --text-font-size:0.85em;
          }
      }
      @media only screen and (min-width: 500px) {
          :host {
              --text-font-size:0.95em;
              --flex-colheight: 200px;
          }
      }
      @media only screen and (min-width: 700px) {
          :host {
              --text-font-size:1.05em;
              --max-width:600px;
          }
      }
      @media only screen and (min-width: 900px) {
          :host {
              --text-font-size:0.95em;
              --max-width:350px;
              --flex-colheight: 220px;
          }
      }

      .post-preview {
        border-bottom: 1px solid var(--primary-lines-color, #ccc);
        max-width: var(--max-width);
        box-shadow: var(--primary-color) 0.2em 0.2em 0.4em;
        border-radius: 10px;
        margin: 1em auto;
      }

      header {
        font-family: var(--ml-header-font-family);
      }

      img {
          width: 100%;
          height: 100%;
          object-fit: cover;       /* Scale the image so it covers whole area, thus will likely crop */
      }

      .flexcol-center {
          display: flex;
          flex-flow: column nowrap;
          justify-content: center;
          align-items: center;
          overflow: hidden;
      }

      .img-box {
         /*background-color: rgba(206, 229, 246, 0.5);*/
         padding: 1em;
      }

      .flexcol-item {
          width: 90%;
          height:var(--flex-colheight);
          overflow: hidden;
      }

      .flexcol-text {
          overflow-y: auto;
          margin-top:1em;
      }

      a.link-box {
          text-decoration: none;
      }

      a.link-box:hover {
          background-color: rgba(0, 0, 0, 0.03);
      }

      .title,
      .author,
      .publish-date {
        margin:0;
      }

      .author,
      .publish-date {
        padding-top: 0.6em;
      }

      .publish-date,
      .author {
        font-size: var(--date-font-size);
        color: var(--text-color-lighter, #ccc);
      }

      .title {
        font-size: var(--title-font-size);
        font-weight: 700;
        color: var(--primary-color);
        line-height: 1.05em;
      }

      a {
        color: inherit;
        text-decoration: none;
      }
      a:hover {
        color: var(--primary-color);
      }

      #description {
        margin:0;
        margin-top:0.5em;
        font-size: var(--text-font-size);
        line-height: calc(var(--text-font-size) * 1.5);
      }
    `,
  ];
}
