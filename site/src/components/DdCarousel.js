import { __decorate } from "tslib";
import { LitElement, html, css } from 'lit';
import { property, state, queryAssignedElements } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';
/*---------------------------------------------------------------------
 * Constants
 *---------------------------------------------------------------------*/
const BOOTSTRAP_CHEVRON_RIGHT = html `<svg
  xmlns="http://www.w3.org/2000/svg"
  width="16"
  height="16"
  fill="currentColor"
  class="bi bi-chevron-right"
  viewBox="0 0 16 16"
>
  <path
    fill-rule="evenodd"
    stroke="currentColor"
    d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5
       0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"
  />
</svg>`;
const BOOTSTRAP_CHEVRON_LEFT = html `<svg
  xmlns="http://www.w3.org/2000/svg"
  width="16"
  height="16"
  fill="currentColor"
  class="bi bi-chevron-left"
  viewBox="0 0 16 16"
>
  <path
    fill-rule="evenodd"
    stroke="currentColor"
    d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5
       0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"
  />
</svg>`;
const SLIDE_OUT_BACK_LEFT = [
    { transform: 'translateX(0)' },
    { transform: 'translateX(-100vw)' },
];
const SLIDE_OUT_BACK_RIGHT = [
    { transform: 'translateX(0)' },
    { transform: 'translateX(100vw)' },
];
const FORWARD_ANIMATION_OPTS = {
    duration: 500,
    easing: 'ease-in-out',
    iterations: 1,
};
const REVERSE_ANIMATION_OPTS = {
    ...FORWARD_ANIMATION_OPTS,
    direction: 'reverse',
};
const SLIDE_LEFT_OUT = [
    SLIDE_OUT_BACK_LEFT,
    FORWARD_ANIMATION_OPTS,
];
const SLIDE_RIGHT_OUT = [
    SLIDE_OUT_BACK_RIGHT,
    FORWARD_ANIMATION_OPTS,
];
const SLIDE_LEFT_IN = [
    SLIDE_OUT_BACK_LEFT,
    REVERSE_ANIMATION_OPTS,
];
const SLIDE_RIGHT_IN = [
    SLIDE_OUT_BACK_RIGHT,
    REVERSE_ANIMATION_OPTS,
];
/*---------------------------------------------------------------------
 * Helper functions
 *---------------------------------------------------------------------*/
function getMaxElHeight(els) {
    return Math.max(0, ...els.map(el => el.getBoundingClientRect().height));
}
function hideSlide(el) {
    el.classList.add('slide-hidden');
}
function showSlide(el) {
    el.classList.remove('slide-hidden');
}
function wrapIndex(idx, max) {
    return ((idx % max) + max) % max;
}
function unify(e) {
    if (window.TouchEvent && e instanceof TouchEvent)
        return e.changedTouches[0];
    return e;
}
/*---------------------------------------------------------------------
 * Main class
 *---------------------------------------------------------------------*/
export class DdCarousel extends LitElement {
    /**
     * Initialise
     */
    constructor() {
        super();
        // Assume this is always a valid slide index.
        this.horPosStart = null;
        this.dotBtnsHtml = '';
        this.carouselInterval = null;
        this.animDuration = 500;
        this.slideIndex = 0;
        this.noDots = false;
        this.noButtons = false;
        this.autoSlide = 0;
        this.noAnim = false;
        /* set these on the slides element */
        /*this.addEventListener('mousedown', this.lockHorizontalPos, false);*/
        /*this.addEventListener('touchstart', this.lockHorizontalPos, false);*/
    }
    /**
     * Return slide index in the range of [0, slideElement.length)
     */
    get wrappedIndex() {
        return wrapIndex(this.slideIndex, this.slideElements.length);
    }
    generateSlideDotsHtml() {
        for (let i = 0; i < this.slideElements.length; i++) {
            if (i === this.wrappedIndex)
                this.dotBtnsHtml += `<span id="dot-${i}"
                                   part="dot dot-active"
                                   class="dot dot-active">
                             </span>`;
            else
                this.dotBtnsHtml += `<span id="dot-${i}"
                                   part="dot"
                                   class="dot">
                             </span>`;
        }
    }
    initializeSlides() {
        for (let i = 0; i < this.slideElements.length; i++) {
            const slide = this.slideElements[i];
            slide.getAnimations().forEach(anim => anim.cancel());
            if (i === this.wrappedIndex) {
                showSlide(slide);
            }
            else {
                hideSlide(slide);
            }
        }
    }
    lockHorizontalPos(e) {
        this.horPosStart = unify(e).clientX;
    }
    swipe(e) {
        if (this.horPosStart || this.horPosStart === 0) {
            let dx = unify(e).clientX - this.horPosStart;
            this.horPosStart = null;
            if (dx > 0) {
                this.slideIndex -= 1;
            }
            else if (dx < 0) {
                this.slideIndex += 1;
            }
        }
    }
    firstUpdated() {
        var _a, _b;
        this.addEventListener('mouseup', this.swipe, false);
        this.addEventListener('touchend', this.swipe, false);
        if (this.noButtons) {
            for (const btnEl of (_a = this.shadowRoot) === null || _a === void 0 ? void 0 : _a.querySelectorAll('.btn'))
                btnEl.style.display = 'none';
        }
        if (this.noDots)
            ((_b = this.shadowRoot) === null || _b === void 0 ? void 0 : _b.querySelector('#dots')).style.display = 'none';
        if (this.noAnim)
            this.animDuration = 0;
        const compStyles = window.getComputedStyle(this);
        if (!compStyles.getPropertyValue('--slide-height'))
            this.style.setProperty('--slide-height', `${getMaxElHeight(this.slideElements)}px`);
        this.generateSlideDotsHtml();
        this.initializeSlides();
        if (this.autoSlide > 0)
            this.carouselInterval = setInterval(() => {
                this.slideIndex++;
            }, this.autoSlide);
    }
    updated(changedProperties) {
        // Not covered in the video, but if you want to drive animations from the
        // 'slideindex' attribute and property, we can calculate the animation in
        // the 'updated' lifecycle callback.
        if (changedProperties.has('slideIndex')) {
            const oldSlideIndex = changedProperties.get('slideIndex');
            if (oldSlideIndex === undefined) {
                return;
            }
            const isAdvancing = this.slideIndex > oldSlideIndex;
            const slideDiff = this.slideIndex - oldSlideIndex;
            if (isAdvancing) {
                // Animate forwards
                this.navigateWithAnimation(slideDiff, SLIDE_LEFT_OUT, SLIDE_RIGHT_IN);
            }
            else {
                // Animate backwards
                this.navigateWithAnimation(slideDiff, SLIDE_RIGHT_OUT, SLIDE_LEFT_IN);
            }
        }
    }
    navigateToNextSlide() {
        // Animation driven by the `updated` lifecycle.
        this.slideIndex += 1;
    }
    navigateToPrevSlide() {
        // Animation driven by the `updated` lifecycle.
        this.slideIndex -= 1;
    }
    navigateToSlideFromDotBtn(e) {
        if (this.carouselInterval)
            clearInterval(this.carouselInterval);
        const targetElId = e.target.id;
        if (targetElId.substring(0, 4) === 'dot-') {
            if (targetElId.substring(4)) {
                this.slideIndex = Number(targetElId.substring(4));
                //this.navigateWithAnimation(1, SLIDE_LEFT_OUT, SLIDE_RIGHT_IN);
            }
        }
    }
    async navigateWithAnimation(nextSlideOffset, leavingAnimation, enteringAnimation) {
        var _a, _b, _c, _d;
        this.initializeSlides();
        const leavingElIndex = wrapIndex(this.slideIndex - nextSlideOffset, this.slideElements.length);
        const elLeaving = this.slideElements[leavingElIndex];
        showSlide(elLeaving);
        (_b = (_a = this.shadowRoot) === null || _a === void 0 ? void 0 : _a.querySelector(`#dot-${leavingElIndex}`)) === null || _b === void 0 ? void 0 : _b.classList.remove('dot-active');
        // Animate out current element
        const leavingAnim = elLeaving.animate(leavingAnimation[0], { ...leavingAnimation[1], duration: this.animDuration });
        // Entering slide
        const newSlideEl = this.slideElements[this.wrappedIndex];
        (_d = (_c = this.shadowRoot) === null || _c === void 0 ? void 0 : _c.querySelector(`#dot-${this.wrappedIndex}`)) === null || _d === void 0 ? void 0 : _d.classList.add('dot-active');
        // Show the new slide
        showSlide(newSlideEl);
        // Teleport it out of view and animate it in
        const enteringAnim = newSlideEl.animate(enteringAnimation[0], { ...enteringAnimation[1], duration: this.animDuration });
        try {
            // Wait for animations
            await Promise.all([leavingAnim.finished, enteringAnim.finished]);
            // Hide the element that left
            hideSlide(elLeaving);
        }
        catch {
            /* Animation was cancelled */
        }
    }
    render() {
        return html `
      <!-- flex host row 1 -->
      <div id="slide-container" part="slide-container">
        <div
          id="left-button"
          part="buttons left-button"
          exportparts="internal-btn : buttons"
          @click=${this.navigateToPrevSlide}
        >
          <div
            part="internal-btn"
            class="btn"
            tabindex="0"
            role="button"
            aria-label="l-btn"
            aria-pressed="false"
          >
            ${BOOTSTRAP_CHEVRON_LEFT}
          </div>
        </div>
        <div
          part="slides"
          id="slides"
          @mousedown="${this.lockHorizontalPos}"
          @touchstart="${this.lockHorizontalPos}"
        >
          <slot></slot>
        </div>
        <div
          id="right-button"
          part="buttons right-button"
          exportparts="internal-btn : buttons"
          @click=${this.navigateToNextSlide}
        >
          <div
            part="internal-btn"
            class="btn"
            tabindex="0"
            role="button"
            aria-label="r-btn"
            aria-pressed="false"
          >
            ${BOOTSTRAP_CHEVRON_RIGHT}
          </div>
        </div>
      </div>
      <!-- flex host row 2 -->
      <div id="dots" @click="${this.navigateToSlideFromDotBtn}" part="dots">
        ${unsafeHTML(this.dotBtnsHtml)}
      </div>
    `;
    }
}
DdCarousel.styles = css `
    :host {
      display: block;
      width: 100%;
      height: 400px;
      margin: 3em auto 4em auto;

      gap: var(--dd-carousel-dots-gap-vertical, 30px);

      --main-color: var(--dd-carousel-main-color, rgba(5, 47, 95, 1));

      --slide-width: var(--dd-carousel-slide-width, 100%);
      --slide-height: var(--dd-carousel-slide-height, 100%);

      --carousel-border-radius: var(--dd-carousel-border-radius, 15px);

      --carousel-slide-btn-gap: var(--dd-carousel-slide-btn-gap, 10px);
      --carousel-slide-btn-size: var(--dd-carousel-slide-btn-size, 20px);

      --carousel-box-shadow: var(
        --dd-carousel-box-shadow,
        var(--main-color) 0.2em 0.2em 0.4em
      );

      --carousel-dots-size: var(--dd-carousel-dots-size, 7px);
      --carousel-dots-size-active: var(--dd-carousel-dots-size-active, 9px);
      --carousel-dots-gap: var(--dd-carousel-dots-gap, 6px);
      --carousel-dots-bg-color: var(
        --dd-carousel-dots-bg-color,
        var(--main-color)
      );

      --carousel-btn-color: var(--dd-carousel-btn-color, var(--main-color));
      --carousel-btn-background-color: var(
        --dd-carousel-btn-background-color,
        none
      );
      --carousel-btn-box-shadow: var(--dd-carousel-btn-box-shadow, none);
      --carousel-active-btn-color: var(
        --dd-carousel-active-btn-color,
        var(--main-color)
      );
      --carousel-active-btn-background-color: var(
        --dd-carousel-active-btn-background-color,
        none
      );
      --carousel-active-btn-box-shadow: var(
        --dd-carousel-active-btn-box-shadow,
        none
      );

      --carousel-slide-padding: var(--dd-carousel-slide-padding, 10px);
      --carousel-slot-padding: var(--dd-carousel-slot-padding, 0);
    }

    ::slotted(.slide-hidden) {
      display: none;
    }

    /** So the elements all overlap */
    ::slotted(*) {
      position: absolute;
      max-height:95%;
      max-width:95%;
      padding: var(--carousel-slot-padding);
      overflow:auto;
      /*??
      width: var(--slot-width, 100%);
      height: var(--slot-height, auto);
      */
    }

    #slide-container {
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: center;
      gap: var(--carousel-slide-btn-gap);
      padding-bottom:25px;
      height:100%;
    }

    #slides {
      overflow:hidden;
      display: flex;
      align-items: center;
      justify-content: center;
      flex: 1;

      width: var(--slide-width);
      height: var(--slide-height);

      border-radius: var(--carousel-border-radius);

      padding: var(--carousel-slide-padding);
      overflow: hidden;
      position: relative;

      box-shadow: var(--carousel-box-shadow);
    }

    .btn {
      width: calc(1.5 * var(--carousel-slide-btn-size));
      height: calc(1.5 * var(--carousel-slide-btn-size));
      cursor: pointer;
      border-radius: 50%;

      display: flex;
      align-items: center;
      justify-content: center;

      color: var(--carousel-btn-color);
      box-shadow: var(--carousel-btn-box-shadow);
      background-color: var(--carousel-btn-background-color);
    }

    .btn:active {
      box-shadow: var(--carousel-active-btn-box-shadow);
      background-color: var(--carousel-active-btn-background-color);
      color: var(--carousel-active-btn-color);
    }

    @media screen and (max-width: 450px) {
        .btn {
            display:none;
        }
    }

    svg {
      width: var(--carousel-slide-btn-size);
      height: var(--carousel-slide-btn-size);
    }

    #dots {
      display: flex;
      align-items: center;
      justify-content: center;
      gap: var(--carousel-dots-gap);
    }

    .dot {
      cursor: pointer;
      height: var(--carousel-dots-size);
      width: var(--carousel-dots-size);
      background-color: none;
      border: 2px solid rgba(0, 0, 0, 0.3);
      border-radius: 50%;
    }

    .dot-active {
      background-color: var(--carousel-dots-bg-color);
      height: var(--carousel-dots-size-active);
      width: var(--carousel-dots-size-active);
    }
  `;
__decorate([
    state()
], DdCarousel.prototype, "horPosStart", void 0);
__decorate([
    state()
], DdCarousel.prototype, "dotBtnsHtml", void 0);
__decorate([
    state()
], DdCarousel.prototype, "carouselInterval", void 0);
__decorate([
    state()
], DdCarousel.prototype, "animDuration", void 0);
__decorate([
    property({ type: Number, attribute: 'slide-index' })
], DdCarousel.prototype, "slideIndex", void 0);
__decorate([
    property({ type: Boolean, reflect: true, attribute: 'no-dots' })
], DdCarousel.prototype, "noDots", void 0);
__decorate([
    property({ type: Boolean, reflect: true, attribute: 'no-buttons' })
], DdCarousel.prototype, "noButtons", void 0);
__decorate([
    property({ type: Number, reflect: true, attribute: 'auto-slide' })
], DdCarousel.prototype, "autoSlide", void 0);
__decorate([
    property({ type: Boolean, reflect: true, attribute: 'no-anim' })
], DdCarousel.prototype, "noAnim", void 0);
__decorate([
    queryAssignedElements()
], DdCarousel.prototype, "slideElements", void 0);
//# sourceMappingURL=DdCarousel.js.map
