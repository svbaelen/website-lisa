export function tocObserver() {
  // (how come?), the height of the rocket components are only available
  // after some small timeout, even though there should be invoked on the
  // server side, so for now hardcode some stuff (rather than setting an actual
  // timeout).
  const headerHeight = 70;
  const observerOffset = window.innerHeight - 70 - 100;

  const observerOpts =  {
    //root: document.querySelector('rocket-content-area'), // also does not
    //root: null,
    //work
    //rootMargin: `-${observerOffset}px 0px`,
    rootMargin: `0% 0px -${observerOffset}px 0px`,
    threshold: 0
  };
  const selectors = "main h2, main h3";

  const observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      const id = entry.target.getAttribute('id');
      const qString = "nav[data-type='toc'] a[href='#" + id + "']";
      const section = document.querySelector(qString);
      if (entry.intersectionRatio > 0){
        section.classList.add('active');
        const qStringNot = "nav[data-type='toc'] a:not([href='#" + id + "'])";
        const others = document.querySelectorAll(qStringNot);
        others.forEach((otherSection) => {
          otherSection.classList.remove('active');
        });
      }
      else if (window.scrollY < observerOffset){
        section.classList.remove('active');
        //console.log(entry.target);
      }
    });
  }, observerOpts);

  document.querySelectorAll(selectors).forEach((section) => {
    observer.observe(section);
  });
}
