export function activeHeader() {
  const curPageEl = document.querySelector("rocket-header a[aria-current=page]");
  //console.log(curPageEl.attributes);
  //console.log(curPageEl.attributes.href);
  const baseRoute = curPageEl.attributes.href.nodeValue.split("/")[1];

  document.querySelectorAll("rocket-header a").forEach((item) => {
    if (item.attributes.href.nodeValue.includes(`/${baseRoute}`))
        item.setAttribute("aria-current", "page");
  });
}
