# Mappalink public website

## Online

- [https://mappalink.gitlab.io/management/website/ml-public](https://mappalink.gitlab.io/management/website/ml-public)
- [https://mappa.link](https://mappa.link)

## Development

```sh
yarn install
yarn start
# yarn build
```
